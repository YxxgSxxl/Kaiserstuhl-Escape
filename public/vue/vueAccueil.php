<?php
$title = "Kaiserstuhl Outdoor";
?>
<section class="h-[60vh] md:h-[100vh] bg-cover bg-no-repeat" style="background-image: url('img/ks-bg1.png');">

    <?php include "./components/header_notlogged.html"; ?>


    <h1 class="text-white font-semibold mt-0 pt-40 text-4xl md:text-6xl text-center my-40 select-none">VOTRE <span
            class="text-ks-orange">AVENTURE</span><br><span class="text-ks-orange">EN</span> EXTERIEUR
    </h1>
</section>

<section class="h-[100vh] bg-ks-black">
    <div>
        <div class="flex">
            <h2 class="text-ks-white text-4xl">In Vino Veritas - Trailer</h2>
            <img src="" alt="">
        </div>

    </div>
</section>

<section class="h-[100vh] bg-ks-black">

</section>

<section
    class="h-[20vh] bg-ks-grey px-4 text-xs md:px-64 md:py-6 md:text-lg text-ks-white flex justify-between align-middle items-center">
    <div class="flex flex-col align-middle items-center text-center gap-2">
        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 24 24" fill="none" stroke="#f2f2f2"
            stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
            <circle cx="12" cy="12" r="10" />
            <path d="M16.2 7.8l-2 6.3-6.4 2.1 2-6.3z" />
        </svg>
        <p>L'AVENTURE</p>
    </div>
    <div class="flex flex-col align-middle items-center text-center gap-2">
        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 24 24" fill="none" stroke="#f2f2f2"
            stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
            <path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path>
            <circle cx="9" cy="7" r="4"></circle>
            <path d="M23 21v-2a4 4 0 0 0-3-3.87"></path>
            <path d="M16 3.13a4 4 0 0 1 0 7.75"></path>
        </svg>
        <p>TEAM BUILDING</p>
    </div>
    <div class="flex flex-col align-middle items-center text-center gap-2">
        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 24 24" fill="none" stroke="#f2f2f2"
            stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round">
            <rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect>
            <path d="M7 11V7a5 5 0 0 1 9.9-1"></path>
        </svg>
        <p>MYSTERES</p>
    </div>
</section>

<section class="h-[100vh] bg-ks-black">

</section>

<script src="js/script.js"></script>