<?php
$title = "Kaiserstuhl - Jeux";
?>
<section class="h-[100vh] bg-cover bg-no-repeat" style="background-image: url('img/ks-bg1.png');">

    <?php include "./components/header_notlogged.html"; ?>

    <h1 class="text-ks-white text-2xl mt-10 mb-8 md:mt-16 md:mb-8 text-center">
        NOS <span class="text-ks-orange">JEUX</span>
    </h1>
</section>